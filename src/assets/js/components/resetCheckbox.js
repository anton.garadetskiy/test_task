var clearButton = document.querySelector('.clear');
clearButton.onclick = checkClear;

function checkClear(){
	var allInputs = document.getElementsByTagName("input");
for (var i = 0, max = allInputs.length; i < max; i++){
    if (allInputs[i].type === 'checkbox')
        allInputs[i].checked = false;
	}
}